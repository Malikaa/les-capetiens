//creation du serveur
var fs = require("fs"); // fs module qui permet de lire et ecrire dans des fichiers;
let express = require("express");
let app = express();// fontion pour cree le serveur
app.listen(8080);

app.set('view engine', 'ejs');

app.use(express.urlencoded());// récuperer les données envoyées dans le <form> du html
//app.use(express.json());

app.get("/", function (request, response) {
  response.render("template", { page: 'index' });
});

app.get("/hugues-capet", (request, response) => {
  response.render("template", { page: "hugues_capet" })
});

app.get("/louis-ix", (request, response) => {
  response.render("template", { page: "louis_ix" })
});

app.get("/philippe-auguste", (request, response) => {
  response.render("template", { page: "philippe_auguste",active:"active" })
});

app.get("/philippe-lebel", (request, response) => {
  response.render("template", { page: "philippe_lebel" })
});

app.get("/adhesion", (request, response) => {
  response.render("template", { page: "adherer" })
});


// recupere les info envoyées
app.post("/adhesion", (request, response) => {
  let client = request.body;
  let json = JSON.stringify(client); // tronsormer l'objeet en format json 
  // request.body conteint les informations envoyé par le <form>
  // sous forme de json
  fs.appendFile(__dirname + "\\clients.json", json, function (err) {// appendFile permert d'ajouter a la fin du fichier
  });

  response.redirect("/confirmation-adhesion");//rederiger vers la page de confirmation(/confirmation-adhesion)
});

app.get("/confirmation-adhesion", (request, response) => {
  response.render("template", { page: "confirmation-adhesion" })
});

// lorsque le client envoie une requete pour un fichier static (css/js/img) le serveur va les chercher 
// dans les répertoire /css, /js ,/img et les enoyer automatiquement au client
app.use(express.static("css"));
app.use(express.static("js"));
app.use(express.static("img"));


app.get("*", function (request, response) {
  response.status(404).send("Not found");// *(404) doit etre a la fin de toutes les routes
});

